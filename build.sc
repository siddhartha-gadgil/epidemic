import mill._
import mill.scalalib._, mill.scalajslib._

object epidemic extends SbtModule {
  def scalaVersion = "2.13.1"
  def ivyDeps = Agg(
    // ivy"com.lihaoyi::os-lib:0.6.2",
    ivy"com.lihaoyi::upickle:1.0.0",
    ivy"com.lihaoyi::cask:0.5.6",
    ivy"io.monix::monix::3.1.0",
    ivy"org.scalanlp::breeze:1.0"
  )

  def resources = T.sources {
    def base: Seq[os.Path] = super.resources().map(_.path)
    def jsout = client.fastOpt().path / os.up
    (base ++ Seq(jsout)).map(PathRef(_))
  }

  
}

object client extends ScalaJSModule with SbtModule {
  def scalaJSVersion = "1.0.1"

  def scalaVersion = "2.13.1"

  override def ivyDeps = Agg(
    ivy"org.scala-js::scalajs-dom::1.0.0",
    ivy"com.lihaoyi::upickle::1.0.0",
    ivy"com.lihaoyi::scalatags::0.8.6"
  )
}
