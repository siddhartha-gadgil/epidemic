package epidemic
import epidemic.CovidState.Symptomatic

sealed trait Intervention {
  def kappa(state: CovidState, location: String, isCompliant: Boolean): Double
  def kappaIncoming(
      state: CovidState,
      location: String,
      isCompliant: Boolean
  ): Double
}

object Intervention {
  val symptomRecognition = 4

  def isQuarantined(state: CovidState, period: Double) = state match {
    case Symptomatic(timeFromTransition) =>
      (timeFromTransition >= symptomRecognition) && (timeFromTransition <= (symptomRecognition + period))
    case _ => false
  }

  case object NoIntervention extends Intervention {
    def kappa(
        state: CovidState,
        location: String,
        isCompliant: Boolean
    ): Double = 1

    def kappaIncoming(
        state: CovidState,
        location: String,
        isCompliant: Boolean
    ): Double = 1

  }

  case object Lockdown extends Intervention {
    def kappa(
        state: CovidState,
        location: String,
        isCompliant: Boolean
    ): Double =
      if (isCompliant) {
        Map(
          "house" -> 2.0,
          "community" -> 0.25,
          "work" -> 0.25,
          "school" -> 0.0
        )(location)
      } else {
        Map(
          "house" -> 1.25,
          "community" -> 1.0,
          "work" -> 0.25,
          "school" -> 0.0
        )(location)
      }

    def kappaIncoming(
        state: CovidState,
        location: String,
        isCompliant: Boolean
    ): Double =
      if (isCompliant) {
        Map(
          "house" -> 1.0,
          "community" -> 0.25,
          "work" -> 0.25,
          "school" -> 0.0
        )(location)
      } else {
        Map(
          "house" -> 1.0,
          "community" -> 1.0,
          "work" -> 0.25,
          "school" -> 0.0
        )(location)
      }

  }

  val selfIsolation = 7

  case object CaseIsolation extends Intervention {
    def kappa(
        state: CovidState,
        location: String,
        isCompliant: Boolean
    ): Double =
      if (isCompliant && isQuarantined(state, selfIsolation))
        Map(
          "house" -> 0.25,
          "community" -> 0.25,
          "work" -> 0.25,
          "school" -> 0.25
        )(location)
      else
        1

    def kappaIncoming(
        state: CovidState,
        location: String,
        isCompliant: Boolean
    ): Double = if (isCompliant && isQuarantined(state, selfIsolation))
        Map(
          "house" -> 0.25,
          "community" -> 0.25,
          "work" -> 0.25,
          "school" -> 0.25
        )(location)
      else
        1
  }

  case object HomeQuarantine extends Intervention {
    def kappa(
        state: CovidState,
        location: String,
        isCompliant: Boolean
    ): Double =
      if (isCompliant && isQuarantined(state, selfIsolation))
        Map(
          "house" -> 2.0,
          "community" -> 0.25,
          "work" -> 0.25,
          "school" -> 0.25
        )(location)
      else
        1

    def kappaIncoming(
        state: CovidState,
        location: String,
        isCompliant: Boolean
    ): Double = if (isCompliant && isQuarantined(state, selfIsolation))
        Map(
          "house" -> 1.0,
          "community" -> 0.25,
          "work" -> 0.25,
          "school" -> 0.25
        )(location)
      else
        1
  }

  def fromJson(js: ujson.Value) = {
    val obj = js.obj
    obj("intervention").str match {
      case "no-intervention" => NoIntervention
      case "lockdown"        => Lockdown
      case "home-quarantine" => HomeQuarantine
      case "case-isolation" => CaseIsolation
    }
  }
}
