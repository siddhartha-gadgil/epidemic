package epidemic 
import epidemic.CovidState.Susceptible
import epidemic.CovidState.Incubating
import epidemic.CovidState.Symptomatic
import epidemic.CovidState.Hospitalized
import epidemic.CovidState.Critical
import epidemic.CovidState.Recovered
import epidemic.CovidState.Deceased
import epidemic.CovidState.Infective

/**
  * The state of an individual with respect to COVID 19 infection. 
  */
sealed trait CovidState 

/**
  * Those Covid19 states where the individual has been exposed. This sub-class is defined as the further evolution depends only on the individual
  */
sealed trait Exposed extends CovidState

object CovidState{
    /**
      * Not yet exposed, hence susceptible to the infection
      */
    case object Susceptible extends CovidState

    /**
      * Caught the infection and incubating. To model Gamma(2, _) distribution for the incubation time, we treat this as phases which are either early or not.
      *
      * @param timeFromTransition time from reaching this state
      * @param early whether in the first phase of inubation
      */
    case class Incubating(timeFromTransition: Double, early: Boolean) extends Exposed

    /**
      * The pre-symptomatic/asymptomatic phase where the individual is infective but shows no symptoms
      *
      * @param timeFromTransition
      */
    case class Infective(timeFromTransition: Double) extends Exposed

    case class Symptomatic(timeFromTransition: Double) extends Exposed
    
    case class Hospitalized(timeFromTransition: Double) extends Exposed

    case class Critical(timeFromTransition: Double) extends Exposed

    case object Recovered extends Exposed

    case object Deceased extends Exposed


    def pastTransmission(state: CovidState) = state match {
        case Susceptible => false
        case Incubating(_, _) => false
        case Infective(_) => false
        case Symptomatic(_) => false
        case Hospitalized(_) => true
        case Critical(_) => true
        case Recovered => true
        case Deceased => true
    }

    /**
      * The infectivity of an individual in a given state
      *
      * @param state the COVID state
      * @return infectivity
      */
    def infectivity(state: CovidState) : Double = state match {
        case Susceptible => 0
        case Incubating(_, _) => 0
        case Infective(_) => 1
        case Symptomatic(_) => 1.5
        case Hospitalized(_) => 0
        case Critical(_) => 0
        case Recovered => 0
        case Deceased => 0
    }

    /**
      * name of state for summaries
      *
      * @param state state
      * @return a name
      */
    def name(state: CovidState) : String = state match {
        case Susceptible => "susceptible"
        case Incubating(_, _) => "incubating"
        case Infective(_) => "infective"
        case Symptomatic(_) => "symptomatic"
        case Hospitalized(_) => "hospitalized"
        case Critical(_) => "critical"
        case Recovered => "recovered"
        case Deceased => "deceased"
    }

    /**
      * map COVID states to their groups
      *
      * @param m the map of covid states
      * @return a map of groups of states
      */
    def summary(m: Map[Person, CovidState]) : Map[String, Int] = 
        m.view.mapValues(name(_)).map{case (a, b) => (b, a)}.groupBy(_._1).view.mapValues(_.size).toMap
}