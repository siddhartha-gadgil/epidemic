package epidemic

/**
  * Demography built from city states by parsing JSON files in resources
  */
object CityData{
    def readInputFile(s: String) = 
        scala.io.Source.fromResource(s"input_files/$s").mkString
        //os.read(os.resource / "input_files" / s)

    lazy val schools : Set[School] = 
        ujson.read(readInputFile("schools.json")).arr.map(_("ID").num.toInt).map{Academy(_)}.toSet

    lazy val wards : Set[Community] = 
        ujson.read(readInputFile("commonArea.json")).arr.map(_("ID").num.toInt).map{Ward(_)}.toSet

    lazy val houses : Set[Household] = 
        ujson.read(readInputFile("houses.json")).arr.map(_("id").num.toInt).map{House(_)}.toSet

    lazy val offices : Set[Workplace] =
        ujson.read(readInputFile("workplaces.json")).arr.map(_("ID").num.toInt).map{Office(_)}.toSet

    lazy val individualData =
        ujson.read(readInputFile("individuals.json")).arr

    lazy val persons = 
        individualData.map(_("id").num.toInt).map(Individual(_))

    lazy val personSchool : Map[Person, School] = 
        individualData.map(d => (d("id"), d("school"))).filterNot(_._2.isNull).map{
            case (x, y) => (Individual(x.num.toInt), Academy(y.num.toInt))
        }.toMap
    
    lazy val personWorkplace : Map[Person, Workplace] = 
        individualData.map(d => (d("id"), d("workplace"))).filterNot(_._2.isNull).map{
            case (x, y) => (Individual(x.num.toInt), Office(y.num.toInt))
        }.toMap

    lazy val personWard : Map[Person, Community] = 
        individualData.map(d => (d("id"), d("wardNo"))).filterNot(_._2.isNull).map{
            case (x, y) => (Individual(x.num.toInt), Ward(y.num.toInt))
        }.toMap

    lazy val personHouse : Map[Person, Household] = 
        individualData.map(d => (d("id"), d("household"))).filterNot(_._2.isNull).map{
            case (x, y) => (Individual(x.num.toInt), House(y.num.toInt))
        }.toMap

    lazy val demography = Demography(persons.toSet,schools, offices, wards, houses, personSchool,personWorkplace, personWard, personHouse)
}       