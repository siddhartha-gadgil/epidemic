package epidemic

sealed trait Location

object Location{
    case object School extends Location 
    case object Work extends Location
    case object House extends Location
    case object Community extends Location 

    val all = Vector(School, Work, House, Community)
}