package epidemic

class Person

class School

class Workplace

class Community

class Household

case class Ward(id: Int) extends Community

case class Individual(id: Int) extends Person

case class Office(id: Int) extends Workplace

case class House(id: Int) extends Household

case class Academy(id: Int) extends School

/**
  * The Demographic data. This class also computes persons in a school etc. to avoid repeating this computation.
  *
  * @param persons
  * @param schools
  * @param workplaces
  * @param communities
  * @param households
  * @param personSchool
  * @param personWork
  * @param personCommunity
  * @param personHousehold
  */
case class Demography(
    persons: Set[Person],
    schools: Set[School],
    workplaces: Set[Workplace],
    communities: Set[Community],
    households: Set[Household],
    personSchool: Map[Person, School],
    personWork: Map[Person, Workplace],
    personCommunity: Map[Person, Community],
    personHousehold: Map[Person, Household]
) {
  lazy val schoolGroups: Map[School, Vector[Person]] =
    personSchool.groupBy(_._2).view.mapValues(s => s.map(_._1).toVector).toMap

  lazy val schoolSizes: Map[School, Int] =
    personSchool.groupBy(_._2).view.mapValues(_.size).toMap

  lazy val houseGroups: Map[Household, Vector[Person]] =
    personHousehold
      .groupBy(_._2)
      .view
      .mapValues(s => s.map(_._1).toVector)
      .toMap

  lazy val houseSizes: Map[Household, Int] =
    personHousehold.groupBy(_._2).view.mapValues(_.size).toMap

  lazy val workGroups: Map[Workplace, Vector[Person]] =
    personWork.groupBy(_._2).view.mapValues(s => s.map(_._1).toVector).toMap

  lazy val workSizes: Map[Workplace, Int] =
    personWork.groupBy(_._2).view.mapValues(_.size).toMap

  lazy val communityGroups: Map[Community, Vector[Person]] =
    personCommunity
      .groupBy(_._2)
      .view
      .mapValues(s => s.map(_._1).toVector)
      .toMap

  lazy val communitySizes: Map[Community, Int] =
    personCommunity.groupBy(_._2).view.mapValues(_.size).toMap
}

object Demography {
  import Transitions.{random => rnd}
  // Random population, not used since we use the toy Bangalore deomgraphics
  def random(
      numPersons: Int = 500,
      numSchools: Int = 20,
      numWorkplaces: Int = 40,
      numCommunities: Int = 5,
      numHouseholds: Int = 120
  ) = {
    val persons = (0 until (numPersons)).toVector.map((j: Int) => new Person)
    val schools = (0 until (numSchools)).toVector.map((j: Int) => new School)
    val workplaces =
      (0 until (numWorkplaces)).toVector.map((j: Int) => new Workplace)
    val communities =
      (0 until (numCommunities)).toVector.map((j: Int) => new Community)
    val households =
      (0 until (numHouseholds)).toVector.map((j: Int) => new Household)
    val personSchool = persons
      .filter(_ => rnd.nextDouble < 0.5)
      .map(x => x -> schools(rnd.nextInt(numSchools)))
    val personWork = persons
      .filter(_ => rnd.nextDouble < 0.5)
      .map(x => x -> workplaces(rnd.nextInt(numWorkplaces)))
    val personCommunity =
      persons.map(x => x -> communities(rnd.nextInt(numCommunities)))
    val personHousehold =
      persons.map(x => x -> households(rnd.nextInt(numHouseholds)))
    Demography(
      persons.toSet,
      schools.toSet,
      workplaces.toSet,
      communities.toSet,
      households.toSet,
      personSchool.toMap,
      personWork.toMap,
      personCommunity.toMap,
      personHousehold.toMap
    )
  }
}
