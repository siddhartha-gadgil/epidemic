package epidemic
import Transitions.random
import monix.eval._

/**
  * The present state, consisting of the COVID state of persons and demography.
  * The rate of infections in different places is computed here, to avoid recomputing per person.
  * TODO: these rates depend on the intervention, which should be an additional parameter which is used.
  *
  * @param covidState the COVID states of people
  * @param demography population demography
  */
case class PopulationState(
    covidState: Map[Person, CovidState],
    demography: Demography,
    intervention: Intervention,
    compliance: Double
) {
  import CovidState._
  import demography._

  val compliantTask =
    Task
      .gatherUnordered(
        households.map(h => Task(h -> (random.nextDouble() <= compliance)))
      )
      .map(_.toMap)
      .memoize

  // average rate in schools
  val carriersInSchoolTask: Task[Map[School, Double]] =
    Task
      .gatherUnordered {
        schoolGroups.map {
          case (x, v) =>
            compliantTask.map(compl =>
              v.map { p =>
              val weight =
                  intervention.kappa(covidState(p),  "school", compl(personHousehold(p)))
              infectivity(covidState(p)) * weight
            }.sum / schoolSizes(x)
            )
            .map(x -> _)
        }
      }
      .map(_.toMap)
      .memoize

  val carriersInWorkTask: Task[Map[Workplace, Double]] =
    Task
      .gatherUnordered {
        workGroups.map {
          case (x, v) =>
            compliantTask.map(compl =>
              v.map { p =>
              val weight =
                  intervention.kappa(covidState(p),  "work", compl(personHousehold(p)))
              infectivity(covidState(p)) * weight
            }.sum / workSizes(x)
            )
            .map(x -> _)
        }
      }
      .map(_.toMap)
      .memoize

  val carriersInHouseTask: Task[Map[Household, Double]] =
    Task
      .gatherUnordered {
        houseGroups.map {
          case (x, v) =>
            compliantTask.map(compl =>
              v.map { p =>
              val weight =
                  intervention.kappa(covidState(p),  "house", compl(personHousehold(p)))
              infectivity(covidState(p)) * weight
            }.sum / houseSizes(x)
            )
            .map(x -> _)
        }
      }
      .map(_.toMap)
      .memoize

  val carriersInCommunityTask: Task[Map[Community, Double]] =
    Task
      .gatherUnordered {
        communityGroups.map {
          case (x, v) =>
            compliantTask.map(compl =>
              v.map { p =>
              val weight =
                  intervention.kappa(covidState(p),  "community", compl(personHousehold(p)))
              infectivity(covidState(p)) * weight
            }.sum / communitySizes(x)
            )
            .map(x -> _)
        }
      }
      .map(_.toMap)
      .memoize

  // not used
  lazy val nameMap: Map[String, Int] =
    covidState.toVector
      .groupBy {
        case (_, cs) => CovidState.name(cs)
      }
      .view
      .mapValues(_.size)
      .toMap
}
