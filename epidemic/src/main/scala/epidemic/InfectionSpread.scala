package epidemic
import epidemic.Intervention.NoIntervention
import epidemic.CovidState.Susceptible
import epidemic.CovidState.Incubating
import scala.concurrent._
import monix.eval._
import monix.tail._
import CovidState.summary

/**
  * The rate of acquiring infection from various locations
  *
  * @param home from household
  * @param school from school
  * @param work from workplace
  * @param community from community
  */
case class InfectionRate(
    home: Double = 0.47,
    school: Double = 0.752,
    work: Double = 0.376,
    community: Double = 0.3359
)

object InfectionRate {
  def fromJson(js: ujson.Value) = {
    val obj = js.obj
    InfectionRate(
      obj("home").num,
      obj("school").num,
      obj("work").num,
      obj("community").num
    )
  }
}

/**
  * Computation of probabilities of individuals being infected in a given step
  *
  * @param ps population state (should include intervention)
  * @param ir infection rate
  * @param stepsPerDay steps per day
  */
case class InfectionProbability(
    ps: PopulationState,
    ir: InfectionRate,
    stepsPerDay: Int
) {
  import ps._, demography._

  def infectionProbabilityTask(
      person: Person
  ): Task[Vector[(String, Double)]] =
    compliantTask.flatMap { compl =>
      def incInterventionWeight(p: Person, location: String): Double =
          intervention.kappaIncoming(covidState(person),  location, compl(personHousehold(person)))
      val fromSchool = carriersInSchoolTask.map { carriers =>
        personSchool //infection from school
          .get(person)
          .map(x =>
            carriers(x).toDouble * ir.school * incInterventionWeight(
              person,
              "school"
            ) / stepsPerDay
          )
          .getOrElse(0.0) // case with no school
      }

      val fromHome =
        carriersInHouseTask.map { carriers =>
          personHousehold
            .get(person) // infection from household
            .map(x =>
              carriers(x).toDouble * ir.home * incInterventionWeight(
                person,
                "house"
              ) * math
                .pow(houseSizes(x), 0.2) / stepsPerDay // n^0.2 factor
            )
            .getOrElse(0.0)
        }

      val fromWork = carriersInWorkTask.map { carriers =>
        personWork // infection from workplace
          .get(person)
          .map(x =>
            carriers(x).toDouble * ir.work * incInterventionWeight(
              person,
              "work"
            ) / stepsPerDay
          )
          .getOrElse(0.0)
      }

      val fromCommunity = carriersInCommunityTask.map { carriers =>
        personCommunity // infection from community
          .get(person)
          .map(x =>
            carriers(x).toDouble * ir.community * incInterventionWeight(
              person,
              "community"
            ) / stepsPerDay
          )
          .getOrElse(0.0)
      }

      Task.parMap4(fromSchool, fromHome, fromWork, fromCommunity) {
        case (school, house, work, community) =>
          Vector(
            "school" -> school,
            "house" -> house,
            "work" -> work,
            "community" -> community
          )
      }
    }

}

object InfectionSpread {
  import Transitions.random

  def randomInitial(
      rate: Double,
      dem: Demography = CityData.demography
  ): Map[Person, CovidState] =
    dem.persons.map { p =>
      p -> {
        if (random.nextDouble() > rate) Susceptible else Incubating(0, true)
      }
    }.toMap


  implicit val ec: scala.concurrent.ExecutionContext =
    scala.concurrent.ExecutionContext.global

  def taskNextState(
      initMap: Map[Person, CovidState],
      demography: Demography,
      ir: InfectionRate,
      tc: TransitionCoefficients,
      stepsPerDay: Int,
      intervention: Intervention,
      compliance: Double
  ) = {
    val ps = PopulationState(initMap, demography, intervention, compliance)
    val infProbs = InfectionProbability(
      ps,
      ir,
      stepsPerDay
    )
    import Transitions._
    val stateTask = Task.gatherUnordered {
      demography.persons.map { p =>
        {
          val dataTask = {
            initMap(p) match {
              case Susceptible =>
                // val probVec = infProbs.infectionProbabilityVector(p)
                val probTask = infProbs.infectionProbabilityTask(p)
                probTask.map { probVec =>
                  val infections = for {
                    (loc, prob) <- probVec
                    if (random.nextDouble() < prob)
                  } yield loc
                  import demography._
                  val schoolOpt = if (infections.contains("school")) Some(personSchool(p)) else None
                  val workOpt = if (infections.contains("work")) Some(personWork (p)) else None
                  val houseOpt = if (infections.contains("house")) Some(personHousehold (p)) else None
                  val communityOpt = if (infections.contains("community")) Some(personCommunity (p)) else None
                  if (infections.isEmpty) (Susceptible, None, (None, None, None, None))
                  else (Incubating(0, true), Some(infections), (schoolOpt, workOpt, houseOpt, communityOpt))

                }

              case state: Exposed =>
                Task((exposedTransition(state, tc, stepsPerDay), None, (None, None, None, None)))
            }
          }
          dataTask.map{case (d1, d2, d3) => (p,  (d1, d2, d3))}
        }
      }
    }.memoize
    val sources = stateTask.map {
      case list =>
        val sizes = list.toVector
          .flatMap {
            case (_, (_, v, (_, _, _, _))) => v
          }
          .flatten
          .groupBy(identity(_))
          .view
          .mapValues(_.size)
        sizes.toMap
    }

    val schoolFreq = stateTask.map {
      case list =>
        val sizes = list.toVector
          .flatMap {
            case (_, (_, _, (s, _, _, _))) => s
          }
          .groupBy(identity(_))
          .view
          .mapValues(_.size)
        sizes.toMap
    }

    val workFreq = stateTask.map {
      case list =>
        val sizes = list.toVector
          .flatMap {
            case (_, (_, _, (_, w, _, _))) => w
          }
          .groupBy(identity(_))
          .view
          .mapValues(_.size)
        sizes.toMap
    }

    val houseFreq = stateTask.map {
      case list =>
        val sizes = list.toVector
          .flatMap {
            case (_, (_, _, (_, _, h, _))) => h
          }
          .groupBy(identity(_))
          .view
          .mapValues(_.size)
        sizes.toMap
    }

    val communityFreq = stateTask.map {
      case list =>
        val sizes = list.toVector
          .flatMap {
            case (_, (_, _, (_, _, _, c))) => c
          }
          .groupBy(identity(_))
          .view
          .mapValues(_.size)
        sizes.toMap
    }

    val chooserTask = Task.parMap4(schoolFreq, workFreq, houseFreq, communityFreq){
      case (s, w, h, c) => ChooseInfectors(ps, s, h, w, c)
    }

    val choices = chooserTask.flatMap(_.frequencies)

    Task.parZip3(
      stateTask.map(task => task.toMap.view.mapValues(_._1).toMap),
      sources,
      choices
    )
  }

  def summaryIterant(
      initMap: Map[Person, CovidState],
      demography: Demography = CityData.demography,
      ir: InfectionRate = InfectionRate(),
      tc: TransitionCoefficients = TransitionCoefficients(),
      stepsPerDay: Int = 4,
      intervention: Intervention,
      compliance: Double
  ) =
    Iterant.fromLazyStateAction[
      Task,
      (Map[Person, CovidState], Map[Person, Int]),
      (
          Map[
            String,
            Int
          ],
          Map[String, Int],
          Option[Double] // R0 if there is data
      )
    ] { case (cs, culp) =>
      taskNextState(
        cs,
        demography,
        ir,
        tc,
        stepsPerDay,
        intervention,
        compliance
      ).map { case (s, m, freq) =>
          // println(culp.size)
          // println(freq.size)
         ((summary(s), m, RZero.getOpt(culp, cs)), (s, RZero.addMap(culp, freq) )) }
    }(Task.now(initMap -> Map()))

  def cumulativeIterant(
      initMap: Map[Person, CovidState],
      demography: Demography = CityData.demography,
      ir: InfectionRate = InfectionRate(),
      tc: TransitionCoefficients = TransitionCoefficients(),
      stepsPerDay: Int = 4,
      intervention: Intervention,
      compliance: Double
  ) =
    summaryIterant(
      initMap,
      demography,
      ir,
      tc,
      stepsPerDay,
      intervention,
      compliance
    ).scan[ (Vector[(Map[String, Int], Map[String, Int])], Option[Double])]((Vector(), None)) {
      case ((v, _), (s, m, r0)) =>
        // println(m)
        (v :+ (s, m), r0)
    }

}
