package epidemic
import breeze.stats.distributions._
import breeze.linalg._
import CovidState._
import monix.eval._

object RZero {
  def choose(probs: Map[Person, Double], n: Int): Map[Person, Int] = {
    val v = probs.toVector
    val p = DenseVector(v.map(_._2).toArray)

    val mult = Multinomial(p)
    val sample = mult.sample(n).toVector
    sample
      .groupBy(identity(_))
      .view
      .mapValues(_.size)
      .map {
        case (j, p) => v(j)._1 -> p
      }
      .toMap
  }

  def getOpt(culprits: Map[Person, Int], covidState: Map[Person, CovidState]) = {
      val finished = culprits.filter{case (p, n) => CovidState.pastTransmission(covidState(p))}.values

      if (finished.isEmpty) None else Some(finished.sum.toDouble / finished.size)
  }

  def addMap[X](m1: Map[X, Int], m2: Map[X, Int]) =
    (m1.keySet union (m2.keySet)).map(x =>
      x -> (m1.getOrElse(x, 0) + m2.getOrElse(x, 0))
    ).toMap
}

case class ChooseInfectors(
    ps: PopulationState,
    schoolFreq: Map[School, Int],
    homeFreq: Map[Household, Int],
    workFreq: Map[Workplace, Int],
    communityFreq: Map[Community, Int]
) {
  import ps._, demography._, RZero._
  val frequencies : Task[Map[Person,Int]] = Task
    .parZip5(
      compliantTask,
      carriersInSchoolTask,
      carriersInWorkTask,
      carriersInHouseTask,
      carriersInCommunityTask
    )
    .map {
      case (compl, ps, pw, ph, pc) =>
        val schoolChoices = schoolFreq.map {
          case (x, n) => {
              val total = ps(x)
            val personWeights = schoolGroups(x).collect {
              case p if infectivity(covidState(p)) > 0 =>
                val weight =
                    intervention.kappa(covidState(p),  "school", compl(personHousehold(p))) 
                p -> infectivity(covidState(p)) * weight / total
            }.toMap
            choose(personWeights, n)
          }
        }.foldLeft[Map[Person, Int]](Map())(addMap(_, _))

        val workChoices = workFreq.map {
          case (x, n) => {
              val total = pw(x)
            val personWeights = workGroups(x).collect {
              case p if infectivity(covidState(p)) > 0 =>
                val weight =
                    intervention.kappa(covidState(p),  "work", compl(personHousehold(p))) 
                p -> infectivity(covidState(p)) * weight / total
            }.toMap
            choose(personWeights, n)
          }
        }.foldLeft[Map[Person, Int]](Map())(addMap(_, _))

        val houseChoices = homeFreq.map {
          case (x, n) => {
              val total = ph(x)
            val personWeights = houseGroups(x).collect {
              case p if infectivity(covidState(p)) > 0 =>
                val weight =
                    intervention.kappa(covidState(p),  "house", compl(personHousehold(p))) 
                p -> infectivity(covidState(p)) * weight / total
            }.toMap
            choose(personWeights, n)
          }
        }.foldLeft[Map[Person, Int]](Map())(addMap(_, _))

        val communityChoices = communityFreq .map {
          case (x, n) => {
              val total = pc(x)
            val personWeights = communityGroups(x).collect {
              case p if infectivity(covidState(p)) > 0 =>
                val weight =
                    intervention.kappa(covidState(p),  "community", compl(personHousehold(p)))
                p -> infectivity(covidState(p)) * weight / total
            }.toMap
            choose(personWeights, n)
          }
        }.foldLeft[Map[Person, Int]](Map())(addMap(_, _))

        Vector(schoolChoices, workChoices, houseChoices, communityChoices).reduce(addMap(_, _))
    }
}
