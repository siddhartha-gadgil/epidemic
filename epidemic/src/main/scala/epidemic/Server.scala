package epidemic

import io.undertow.websockets.core.WebSockets
import cask.endpoints.{WsActor, WsChannelActor}
import scala.collection.mutable
import scala.concurrent._
import InfectionSpread.ec
import scala.util._


object Server extends cask.MainRoutes {
  override def port: Int = 9090

  override def host: String = sys.env.get("IP").getOrElse("localhost")

  @cask.get("/")
  def home() = {
    """<!DOCTYPE html>
        | <html>
        | <title>Server-client demo</title>
        | <link rel="icon" href="public/IIScLogo.jpg">
        |<link rel="stylesheet" href="public/bootstrap.min.css">
        | <link rel="stylesheet" href="public/extras.css">
        |
        |<body>
        | <div class="container-full">
        |         | 
        | <div class="col-md-10 offset-md-1">
        | <h1> Simulation with server-client setup.</h1>
        | <div id="js-div"></div>
        | </div>
        |
        | </div>
        | <script type="text/javascript" src="public/out.js">
        | </script>
        |
        | <script>
        |   CovidInterface.load()
        | </script>
        | </body>
        | </html>
        """.stripMargin
  }

  @cask.staticResources("/public")
  def staticResourceRoutes() = "."

  def jsonIterant(
      ir: InfectionRate = InfectionRate(),
      tc: TransitionCoefficients = TransitionCoefficients(),
      stepsPerDay: Int,
      intervention: Intervention = Intervention.NoIntervention,
      compliance: Double,
      maxSteps: Int
  ) =
    InfectionSpread
      .cumulativeIterant(InfectionSpread.randomInitial(3.0 / 10000), intervention = intervention, compliance = compliance)
      .map{case (ts, r0) =>
        // println(r0)
        ujson.write(
          ujson.Obj(
            "frequency-time-series" -> upickle.default.write(ts.map(_._1)),
            "infection-sources" -> upickle.default.write(ts.map(_._2)),
            "R0" -> upickle.default.write(r0),
            "total-steps" -> maxSteps
          )
        )        
      }
      .take(maxSteps)

  def runIterant(
       ir: InfectionRate = InfectionRate(),
      tc: TransitionCoefficients = TransitionCoefficients(),
      stepsPerDay: Int,
      intervention: Intervention = Intervention.NoIntervention,
      compliance: Double,
      maxSteps: Int,
      channel: WsChannelActor) = {
    import monix.execution.Scheduler.Implicits.global
    val it = jsonIterant(ir, tc, stepsPerDay, intervention, compliance, maxSteps)
    it.map { x =>
        Try(channel.send(cask.Ws.Text(x)))
      }.takeWhile(attempt => attempt.isSuccess).foreach(_ => ()).runToFuture
    
  }

  @cask.websocket("/simulator")
  def sockStreamer(): cask.WebsocketResult = {
    cask.WsHandler { channel =>
      
      cask.WsActor {
        case cask.Ws.Text(data) =>
            val js = ujson.read(data)
            val stepsPerDay = 4
            val days = 50
            val totalSteps = stepsPerDay * days
            runIterant(InfectionRate.fromJson(js),
                TransitionCoefficients.fromJson(js),
                stepsPerDay,
                intervention = Intervention.fromJson(js),
                maxSteps = totalSteps,
                compliance = js.obj("compliance").num.toDouble,
                channel = channel)
      }
    }
  }

  initialize()
}
