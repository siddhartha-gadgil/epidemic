package epidemic
import scala.util._
import epidemic.CovidState.Incubating
import epidemic.CovidState.Symptomatic
import epidemic.CovidState.Hospitalized
import epidemic.CovidState.Critical
import epidemic.CovidState.Recovered
import epidemic.CovidState.Deceased

/**
  * Parameters for the progression of a disease not including getting the disease. This depends only on the individual
  *
  * @param meanIncubation mean incubation period
  * @param meanPreSymptomaticPeriod mean pre-symptomatic/asymptomatic period
  * @param meanSymptomaticPeriod mean symptomatic period
  * @param meanHospitalStay mean stay in hospital, before recovery or moving to the ICU
  * @param meanICUStay mean stay in ICU, before recovery or death
  * @param symptomaticFraction fraction who become symptomatic
  * @param hospitalizationFraction fraction symptomatic who are hospitalized (rest recover)
  * @param icuFraction fraction hospitalized who become critical (rest recover)
  * @param icuRecoveryFraction fraction who recover after being critical 
  */
case class TransitionCoefficients(
    meanIncubation: Double = 4.5,
    meanPreSymptomaticPeriod: Double = 0.5,
    meanSymptomaticPeriod: Double = 5,
    meanHospitalStay: Double = 8,
    meanICUStay: Double = 8,
    symptomaticFraction: Double = 0.67,
    hospitalizationFraction: Double = 0.3,
    icuFraction: Double = 0.3,    
    icuRecoveryFraction: Double = 0.3
)

object TransitionCoefficients{
    def fromJson(js: ujson.Value) = {
        val obj = js.obj
        TransitionCoefficients(
            obj("mean-incubation-period").num,
            obj("mean-presymptomatic-period").num,
            obj("mean-symptomatic-period").num,
            obj("mean-hospital-stay").num,
            obj("mean-icu-stay").num,
            obj("symptomatic-fraction").num,
            obj("hospitalization-fraction").num,
            obj("icu-fraction").num,
            obj("icu-recovery-fraction").num
        )
    }
}

/**
  * These are transitions that do not include catching the infection, so depend only on the individual.
  * All the transitions are Geometric distributions, with probability the reciprocal of the mean duration
  */
object Transitions{
    import CovidState._    

    /**
      * Generator for random numbers
      */
    val random = new Random

    /**
      * Transition from incubation. Either does not transition or transitions to "late incubation" if in "early incubation" and to "infective" if in late incubation
      *
      * @param state current state
      * @param coeffs transition coefficients
      * @param stepsPerDay steps per day
      * @return new random state
      */
    def fromIncubating(state: Incubating, coeffs: TransitionCoefficients, stepsPerDay: Int) : Exposed = {
        val transitionProbabilty = 2.0 / (coeffs.meanIncubation * stepsPerDay) // the factor of 2 is because of the two phases
        if (random.nextDouble() > transitionProbabilty) Incubating(state.timeFromTransition + 1.0 / stepsPerDay, state.early)
        else if (state.early) Incubating(0, false) else Infective(0)
    }

    /**
      * Transition from infective. Either does not transition or transitions to either recovered or symptomatic
      *
      * @param state current state
      * @param coeffs transition coefficients
      * @param stepsPerDay steps per day
      * @return new random state
      */
    def fromInfective(state: Infective, coeffs: TransitionCoefficients, stepsPerDay: Int) : Exposed = {
        val transitionProbabilty = 1.0 / (coeffs.meanPreSymptomaticPeriod * stepsPerDay)
        if (random.nextDouble() > transitionProbabilty) Infective(state.timeFromTransition + 1.0 / stepsPerDay)
        else if (
            random.nextDouble() > coeffs.symptomaticFraction 
        ) Recovered
        else Symptomatic(0)
    }

    /**
      * Transition from symptomatic. Either does not transition or transitions to either recovered or hospitalized
      *
      * @param state current state
      * @param coeffs transition coefficients
      * @param stepsPerDay steps per day
      * @return new random state
      */
    def fromSymptomatic(state: Symptomatic, coeffs: TransitionCoefficients, stepsPerDay: Int) : Exposed = {
        val transitionProbabilty = 1.0 / (coeffs.meanSymptomaticPeriod * stepsPerDay)
        if (random.nextDouble() > transitionProbabilty) Symptomatic(state.timeFromTransition + 1.0 / stepsPerDay)
        else if (
            random.nextDouble() > coeffs.hospitalizationFraction
        ) Recovered
        else Hospitalized(0)
    }

    /**
      * Transition from hospitalized. Either does not transition or transitions to either recovered or critical
      *
      * @param state current state
      * @param coeffs transition coefficients
      * @param stepsPerDay steps per day
      * @return new random state
      */
    def fromHospitalized(state: Hospitalized, coeffs: TransitionCoefficients, stepsPerDay: Int) : Exposed = {
        val transitionProbabilty = 1.0 / (coeffs.meanHospitalStay * stepsPerDay)
        if (random.nextDouble() > transitionProbabilty) Hospitalized(state.timeFromTransition + 1.0 / stepsPerDay)
        else if (
            random.nextDouble() > coeffs.icuFraction
        ) Recovered
        else Critical(0)
    }

    /**
      * Transition from critical. Either does not transition or transitions to either recovered or deceased
      *
      * @param state current state
      * @param coeffs transition coefficients
      * @param stepsPerDay steps per day
      * @return new random state
      */
    def fromCritical(state: Critical, coeffs: TransitionCoefficients, stepsPerDay: Int) : Exposed = {
        val transitionProbabilty = 1.0 / (coeffs.meanICUStay * stepsPerDay)
        if (random.nextDouble() > transitionProbabilty) Critical(state.timeFromTransition + 1.0 / stepsPerDay)
        else if (
            random.nextDouble() > coeffs.icuRecoveryFraction
        ) Deceased
        else Recovered
    }

    /**
      * Transition from any exposed state by putting together cases
      *
      * @param state current state
      * @param coeffs transition coefficients
      * @param stepsPerDay steps per day
      * @return new random state
      */
    def exposedTransition(state: Exposed, coeffs: TransitionCoefficients, stepsPerDay: Int) = state match {
        case st@ Incubating(timeFromTransition, _) => 
            fromIncubating(st, coeffs, stepsPerDay)
        case st @ Infective(timeFromTransition) => 
            fromInfective(st, coeffs, stepsPerDay)
        case st @ Symptomatic(timeFromTransition) => 
            fromSymptomatic(st, coeffs, stepsPerDay)
        case st @ Hospitalized(timeFromTransition) => 
            fromHospitalized(st, coeffs, stepsPerDay)
        case st @ Critical(timeFromTransition) => 
            fromCritical(st, coeffs, stepsPerDay)
        case Recovered => Recovered
        case Deceased => Deceased
    }
}