# Usage

This is a server for simulation of the spread of a disease as well as a server-client set up in scala (and scala-js). To run this, ensure you have Java 8 installed (or Java 11, but see below) and run the following on a UNIX like system

```bash
./mill epidemic.run
```

You do not need `scala` or `mill` to be installed as there is a bootstrap script that downloads this. If you have Java 11, then a warning is given which you can ignore.

To browse the code, I recommend using VSCode with the `metals` extension, or intelliJ IDEA.
