package epidemic

import scala.scalajs.js.annotation._
import org.scalajs.dom
import scalatags.JsDom.all._
import scala.util._
import org.scalajs.dom.raw.WebSocket
import org.scalajs.dom.raw.MessageEvent
import math.log

object SvgPlot {
  import scalatags.JsDom.svgTags._
  import scalatags.JsDom.svgAttrs.{`type` => _, id => _, _}

  val locations = Vector("school", "house", "work", "community")

  val locationColours = Map(
    "school" -> "cyan",
    "house" -> "magenta",
    "work" -> "orange",
    "community" -> "grey"
  )

  def vecLogLines(v: Vector[Double], size: Int) = {
    val lines = v.zip(v.tail).zipWithIndex.map {
      case ((b1, b2), n) =>
        line(
          x1 := n,
          x2 := n + 1,
          y1 := 100 - (log(b1 + 1) * 100 / log(100000)),
          y2 := 100 - (log(b1 + 1) * 100 / log(100000)),
          stroke := "blue",
          strokeWidth := 2
        )
    }
    rect(width := size, height := 100, stroke := "black", fill := "none") +: lines
  }

  def vecLines(v: Vector[Double], size: Int) = {
    val lines = v.zip(v.tail).zipWithIndex.map {
      case ((b1, b2), n) =>
        line(
          x1 := n,
          x2 := n + 1,
          y1 := 100 - (b1 / 1000),
          y2 := 100 - (b2 / 1000),
          stroke := "blue",
          strokeWidth := 2
        )
    }
    rect(width := size, height := 100, stroke := "black", fill := "none") +: lines
  }

  def linePlot(v: Vector[Double], size: Int) =
    div(
      span("number of persons"),
      svg(
        viewBox := s"0 0 $size 100",
        xmlns := "http://www.w3.org/2000/svg",
        height := 200,
        width := "100%"
      )(
        vecLines(v, size): _*
      )
    )

  def vecOptLines(v: Vector[Option[Double]], colour: String, size: Int) = {
    val lines = v.zip(v.tail).zipWithIndex.flatMap {
      case ((b1Opt, b2Opt), n) =>
        for {
          b1 <- b1Opt
          b2 <- b2Opt
        } yield line(
          x1 := n,
          x2 := (n + 1),
          y1 := 100 - (b1 * 100),
          y2 := 100 - (b2 * 100),
          stroke := colour,
          strokeWidth := 1
        )
    }
    lines
  }

  def allOptLines(fracs: Map[String, Vector[Option[Double]]], size: Int) =
    rect(width := size, height := 100, stroke := "black", fill := "none") +: locations
      .flatMap(s => vecOptLines(fracs(s), locationColours(s), size))

  def lineOptPlot(v: Map[String, Vector[Option[Double]]], size: Int) = div(
    h3("Sources of infections"),
    p("Initially oscillates a lot"),
    svg(
      viewBox := s"0 0 $size 100",
      xmlns := "http://www.w3.org/2000/svg",
      height := 200,
      width := "100%"
    )(
      allOptLines(v, size): _*
    )
  )

  def logLinePlot(v: Vector[Double], size: Int) =
    div(
      span("log of (number of cases + 1)"),
      svg(
        viewBox := s"0 0 $size 100",
        xmlns := "http://www.w3.org/2000/svg",
        height := 200,
        width := "100%"
      )(
        vecLogLines(v, size): _*
      )
    )

  def plotDiv(vecMap: Vector[(String, Vector[Int])], size: Int) = {
    val plots =
      vecMap
        .filterNot {
          case (s, _) => Set("Day", "Step").contains(s)
        }
        .map {
          case (label, v) =>
            div(
              h4(label.capitalize),
              div(`class` := "row")(
                div(`class` := "col-md-6")(linePlot(v.map(_.toDouble), size)),
                div(`class` := "col-md-6")(logLinePlot(v.map(_.toDouble), size))
              )
            ),
        }
    div(p(), div(plots: _*))
  }
}
@JSExportTopLevel("CovidInterface")
object Interface {
  import SvgPlot.locations

  @JSExport
  def load(): Unit = {
    val jsDiv = dom.document.querySelector("#js-div")
    val trDefaults: Vector[(String, Double)] =
      Vector(
        "mean-incubation-period" -> 4.5,
        "mean-presymptomatic-period" -> 0.5,
        "mean-symptomatic-period" -> 5,
        "mean-hospital-stay" -> 8,
        "mean-icu-stay" -> 8,
        "symptomatic-fraction" -> 0.67,
        "hospitalization-fraction" -> 0.3,
        "icu-fraction" -> 0.3,
        "icu-recovery-fraction" -> 0.3
      )

    val trInputVec = trDefaults.map {
      case (s, x) => s -> input(size := "3")(value := x).render
    }

    val trInputMap = trInputVec.toMap

    val trLIs = trInputVec.map { case (s, inp) => tr(td(s + ": "), td(inp)) }

    val infDefaults = Vector(
      "home" -> 0.47,
      "school" -> 0.752,
      "work" -> 0.376,
      "community" -> 0.3359
    )

    val infInputVec = infDefaults.map {
      case (s, x) => s -> input(size := "3")(value := x).render
    }

    val infInputMap = infInputVec.toMap

    val infLIs = infInputVec.map { case (s, inp) => tr(td(s + ": "), td(inp)) }

    val kappaVec = locations.map { s =>
      s -> input(size := "3")(value := 1.0).render
    }

    val kappaMap = kappaVec.toMap

    val kappaLIs = kappaVec.map { case (s, inp) => tr(td(s + ": "), td(inp)) }

    val kappaInVec = locations.map { s =>
      s -> input(size := "3")(value := 1.0).render
    }

    val kappaInMap = kappaInVec.toMap

    val kappaInLIs = kappaInVec.map {
      case (s, inp) => tr(td(s + ": "), td(inp))
    }

    def setInterventions(
        kappa: Map[String, Double],
        kappaInward: Map[String, Double]
    ) = {
      kappa.foreach { case (s, x)       => kappaMap(s).value = x.toString() }
      kappaInward.foreach { case (s, x) => kappaInMap(s).value = x.toString() }
    }

    val interventions
        : Vector[(String, (Map[String, Double], Map[String, Double]))] = Vector(
      "No intervention" -> (
        (
          Map(
            "house" -> 1.0,
            "school" -> 1.0,
            "work" -> 1.0,
            "community" -> 1.0
          ),
          Map(
            "house" -> 1.0,
            "school" -> 1.0,
            "work" -> 1.0,
            "community" -> 1.0
          )
        )
      ),
      "Close schools" -> (
        (
          Map(
            "house" -> 1.0,
            "school" -> 0.0,
            "work" -> 1.0,
            "community" -> 1.0
          ),
          Map(
            "house" -> 1.0,
            "school" -> 0.0,
            "work" -> 1.0,
            "community" -> 1.0
          )
        )
      )
    )

    val interventionVec = interventions.map {
      case (name, (kap, kapIn)) =>
        val b = button(`class` := "btn btn-info")(name).render
        b.onclick = (_) => setInterventions(kap, kapIn)
        b
    }

    val interventionLIs = interventionVec.map { b =>
      li(`class` := "list-inline-item")(b)
    }

    val complianceButton = input(size := "2", value := 90).render

    val daysButton = input(size := "2", value := 50).render

    val stepsButton = input(size := "2", value := 4).render

    val btn =
      button(`type` := "button", `class` := "btn btn-primary btn-lg")(
        "Run simulation"
      ).render

    val wait = span().render

    val resultsDiv = div().render
    val plotView = div(`class` := "col-md-6").render

    val interventionSelect =
      select(`class` := "form-control-lg", `id` := "intervention")(
        option(
          value := "no-intervention"
        )("No intervention"),
        option(
          value := "lockdown"
        )("Lockdown"),
        option(
          value := "case-isolation"
        )("Case Isolation"),
        option(
          value := "home-quarantine"
        )("Home Quarantine")
      ).render

    jsDiv.appendChild(
      div(
        p(
          "This is from the client, which is in scala compiled to js. In the longer run this should be replaced by a Javascript client."
        ),
        div(`class` := "row")(
          div(`class` := "col-md-6")(
            h2("Simulator"),
            p("You can customize various paramenters before running."),
            h3("Disease progression"),
            table(`class` := "table table-bordered")(trLIs: _*),
            h3("Transmission coefficients"),
            table(`class` := "table table-bordered")(infLIs: _*),
            h2("Interventions"),
            h4("Choose intervention"),
            form(div(`class` := "form-group"))(
              interventionSelect
            ),
            p(),
            table(`class` := "table table-bordered")(
              tr(
                td(h4("Compliance percentage for intervention:")),
                td(span(complianceButton), span(" %"))
              ),
              tr(
                td(h4("Number of days in simulation:")),
                td(span(daysButton))
              ),
              tr(
                td(h4("Steps per day in simulation:")),
                td(span(stepsButton))
              )
            ),
            p(),
            div(btn, wait),
            p(),
            resultsDiv
          ),
          plotView
        )
      ).render
    )

    val chat = new WebSocket(s"ws://${dom.document.location.host}/simulator")

    chat.onopen = { (_) =>
      jsDiv.appendChild(
        p("A web socket is now active. The server sends stuff in Json over it").render
      )
    }

    btn.onclick = (_) => {
      val obj = ujson.Obj()
      trDefaults.foreach {
        case (s, x) => obj(s) = trInputMap(s).value.toDouble
      }
      infDefaults.foreach {
        case (s, x) => obj(s) = infInputMap(s).value.toDouble
      }

      obj("compliance") = complianceButton.value.toDouble

      obj("days") = daysButton.value.toDouble

      obj("steps") = stepsButton.value.toDouble

      obj("intervention") = interventionSelect.value

      chat.send(
        ujson.write(
          obj
        )
      )

      wait.appendChild(
        span(span(" "), span(" Waiting for server to start streaming")).render
      )
    }

    def vectorShow(v: Vector[Map[String, Int]], size: Int) = {
      val labels = Vector(
        "susceptible",
        "incubating",
        "infective",
        "symptomatic",
        "hospitalized",
        "critical",
        "recovered",
        "deceased"
      )
      val vecMap: Vector[(String, Vector[Int])] =
        labels.map(l => l -> v.map(m => m.getOrElse(l, 0)))
      val d = div(id := "results")(
        h3("Streamed results"),
        p(
          "Results of the simulation from a scala server streaming in. There are probably bugs. Also eventually replaced by a C/C++ server."
        ),
        table(`class` := "table table-striped")(
          vecMap.map { case (l, v) => tr(td(b(l)))(v.map(td(_)): _*) }
        )
      ).render
      val s = v.map(m => m.get("susceptible").getOrElse(0)).mkString(" ")
      plotView.innerHTML = ""

      plotView.appendChild(SvgPlot.plotDiv(vecMap, size).render)
      resultsDiv.appendChild(d)
    }

    def addMap[X](m1: Map[X, Int], m2: Map[X, Int]) =
      (m1.keySet union (m2.keySet))
        .map(x => x -> (m1.getOrElse(x, 0) + m2.getOrElse(x, 0)))
        .toMap

    def sourceShow(v: Vector[Map[String, Int]], size: Int) = {
      val sumMap = locations.map(l => l -> v.map(m => m.getOrElse(l, 0)).sum)
      val w = v.scan(Map.empty[String, Int])(addMap(_, _))

      val sumVec = w.map(_.toVector.map(_._2).sum)
      val fracMap: Map[String, Vector[Option[Double]]] =
        locations.map { s =>
          s -> w.zip(sumVec).map {
            case (m, total) =>
              if (total == 0) None
              else
                Option(
                  m.getOrElse(s, 0).toDouble / total
                )
          }
        }.toMap
      val sourceTable =
        div(
          h3("Sources of infection"),
          table(`class` := "table table-striped")(
            sumMap.map { case (l, s) => tr(td(b(l)), td(s)) }
          )
        ).render
      resultsDiv.appendChild(SvgPlot.lineOptPlot(fracMap, size).render)
      resultsDiv.appendChild(sourceTable)
    }

    def showR0(r0: Option[Double]) = {
      val message = r0.fold("not enough data")(x => x.toString)
      val view = div(
        h3("Rate of spread (R0)"),
        p(
          "This is the estimated average number of people a given person infected"
        ),
        div(
          strong("R0 (estimated): "),
          span(message),
          p()
        )
      )
      resultsDiv.appendChild(view.render)
    }

    chat.onmessage = (event: MessageEvent) => {
      resultsDiv.innerHTML = ""
      wait.innerHTML = ""
      val msg = event.data.toString()

      val obj = ujson.read(msg).obj
      val r0Popt = obj.get("R0").map(_.str)
      r0Popt.foreach { r0P =>
        val r0 = upickle.default.read[Option[Double]](r0P)
        showR0(r0)

      }
      val totalSteps = obj("total-steps").num.toInt

      val ts =
        ujson.read(msg).obj("frequency-time-series").str
      val v = upickle.default.read[Vector[Map[String, Int]]](ts)
      vectorShow(v, totalSteps)
      val fs = ujson.read(msg).obj("infection-sources").str

      val w = upickle.default.read[Vector[Map[String, Int]]](fs)
      sourceShow(w, totalSteps)

    }

  }
}
